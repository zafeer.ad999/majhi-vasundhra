<header class="main-nav">
    <nav class="h-100">
        <div class="main-navbar h-100">
            <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
            <div id="mainnav" class="h-100">
                <ul class="nav-menu custom-scrollbar">
                    <li class="back-btn">
                        <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
                    </li>
                    <li class="sidebar-main-title">
                        <div>
                            <h6>General </h6>
                        </div>
                    </li>
                    @can('dashboard.view')
                        <li class="dropdown">
                            <a class="nav-link menu-title link-nav {{ request()->routeIs('dashboard') ? 'active-bg' : '' }}" href="{{ route('dashboard') }}">
                                <i data-feather="home"></i><span>Dashboard</span></a>
                        </li>
                    @endcan

                    @can('departments.view')
                        <li class="dropdown">
                            <a class="nav-link menu-title" href="javascript:void(0)">
                                <i data-feather="list"></i><span>Masters</span>
                            </a>
                            <ul class="nav-submenu menu-content">
                                @can('departments.view')
                                    <li><a href="{{ route('departments.index') }}">Questions </a></li>
                                @endcan
                                @can('users.view')
                                    <li><a href="{{ route('users.index') }}">Users </a></li>
                                @endcan
                            </ul>
                        </li>
                    @endcan

                    @can(['roles.view'])
                        <li class="dropdown">
                            <a class="nav-link menu-title" href="javascript:void(0)">
                                <i data-feather="user"></i><span>User Management</span>
                            </a>
                            <ul class="nav-submenu menu-content">
                                @can('users.view')
                                    <li><a href="{{ route('users.index') }}">Users </a></li>
                                @endcan
                                @can('roles.view')
                                    <li><a href="{{ route('roles.index') }}">Roles </a></li>
                                @endcan
                            </ul>
                        </li>
                    @endcan


                    @can('employees.view')
                        <li class="dropdown">
                            <a class="nav-link menu-title" href="javascript:void(0)">
                                <i data-feather="users"></i><span>Employees</span>
                            </a>
                            <ul class="nav-submenu menu-content">
                                @can('employees.create')
                                    <li><a href="{{ route('employees.create') }}">Add </a></li>
                                @endcan
                                @can('employees.view')
                                    <li><a href="{{ route('employees.index') }}">Employees List </a></li>
                                @endcan
                            </ul>
                        </li>
                    @endcan



                    @can('reports.month-wise')
                        <li class="dropdown">
                            <a class="nav-link menu-title" href="javascript:void(0)">
                                <i data-feather="layout"></i><span>Reports</span>
                            </a>
                            <ul class="nav-submenu menu-content">
                                @can('reports.month-wise')
                                    <li><a href="{{ route('reports.index') }}">Month wise report </a></li>
                                @endcan
                                @can('reports.muster')
                                <li><a href="{{ route('reports.muster') }}">Muster report </a></li>
                                @endcan
                            </ul>
                        </li>
                    @endcan


                    <li class="dropdown">
                        <a class="nav-link menu-title link-nav {{ request()->routeIs('show-change-password') ? 'active-bg' : '' }}" href="{{ route('show-change-password') }}">
                            <i data-feather="lock"></i><span>Change Password</span>
                        </a>
                    </li>


                    <li class="dropdown">
                        <a class="nav-link menu-title link-nav {{ request()->routeIs('logout') ? 'active-bg' : '' }}" onclick="event.preventDefault(); document.getElementById('side-logout-form').submit();" href="{{ route('logout') }}">
                            <i data-feather="log-out"></i><span>Logout</span>
                        </a>
                        <form id="side-logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>

                </ul>
            </div>
            <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
        </div>
    </nav>
</header>
