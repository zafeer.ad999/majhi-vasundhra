<x-admin.admin-layout>
    <x-slot name="title">Majhi Vasundara - Dashboard</x-slot>

    <div class="page-body">
        <!-- Container-fluid starts-->
        <div class="container-fluid dashboard-default-sec">

            <div class="row">
                <div class="col-12 px-0">

                    @if ($isAdmin)

                        <div class="row">
                            @foreach ($categoryWiseUsers as $category)
                                <div class="col-sm-6 col-xl-3 col-lg-3">
                                    <div class="card o-hidden border-0">
                                        <div class="bg-blue b-r-4 card-body">
                                            <div class="media static-top-widget">
                                                <div class="media-body"><span class="m-0">{{ $category->name }}</span>
                                                    <h4 class="mb-0 counter"> {{ $category->users->contsts_count }} </h4><i class="icon-bg" data-feather="user"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                    @else

                        <div class="row">
                            @foreach ($departmentWiseData as $department)
                                <div class="col-sm-6 col-xl-3 col-lg-3">
                                    <div class="card o-hidden border-0">
                                        <div class="bg-blue b-r-4 card-body">
                                            <div class="media static-top-widget">
                                                <div class="media-body"><span class="m-0">{{ $department->name }}</span>
                                                    <h4 class="mb-0 counter"> {{ $department->users->contsts_count }} </h4><i class="icon-bg" data-feather="user"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                    @endif

                </div>



            </div>

        </div>
        <!-- Container-fluid Ends-->
    </div>

</x-admin.admin-layout>

