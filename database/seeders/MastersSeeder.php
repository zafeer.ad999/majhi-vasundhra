<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Department;
use App\Models\Shift;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MastersSeeder extends Seeder
{
    use WithoutModelEvents;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //Seed category
        $categories = DB::table('category_master')->orderBy('category_id')->get();
        foreach ($categories as $cat) {
            Category::updateOrCreate([
                'id' => $cat->category_id
            ], [
                'id' => $cat->category_id,
                'name' => $cat->category_name,
            ]);
        }


        //Seed Department
        $departments = DB::table('dept_master')->orderBy('dept_id')->get();
        foreach ($departments as $dep) {
            Department::updateOrCreate([
                'id' => $dep->dept_id
            ], [
                'id' => $dep->dept_id,
                'name' => $dep->dept_name,
            ]);
        }


    }
}
