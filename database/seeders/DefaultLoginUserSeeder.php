<?php

namespace Database\Seeders;

use App\Models\Tenant;
use App\Models\User;
use Carbon\Carbon;
use Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class DefaultLoginUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $tenant = Tenant::updateOrCreate([
            'name'=> 'Majhi Vasundra',
        ],[
            'name'=> 'Majhi Vasundra',
            'address'=> 'Thane - 401 107',
        ]);

        // User Seeder ##
        $makerRole = Role::updateOrCreate(['name'=> 'User', 'tenant_id'=> '1']);
        $permissions = Permission::pluck('id','id')->all();
        $makerRole->syncPermissions($permissions);

        $user = User::updateOrCreate([
            'email' => 'aa@gmail.com'
        ],[
            'competition_type_id' => '1',
            'category_id' => '3',
            'name' => 'User',
            'person_name' => 'User test',
            'email' => 'aa@gmail.com',
            'mobile' => '9876598765',
            'address' => 'lorem ipsum dolor sit amet',
            'pincode' => '123456',
            'ward' => 'lorem ipsum dolor',
            'password' => Hash::make('12345678'),
            'tenant_id' => $tenant->id,
        ]);
        DB::table('model_has_roles')->updateOrInsert([
            'model_type'=> 'App\Models\User',
            'model_id'=> $user->id,
        ],[
            'role_id'=> $makerRole->id,
            'tenant_id'=> $tenant->id
        ]);



        // Super Admin Seeder ##
        $superAdminRole = Role::updateOrCreate(['name' => 'Super Admin','tenant_id' => 1]);
        $permissions = Permission::pluck('id','id')->all();
        $superAdminRole->syncPermissions($permissions);

        $user = User::updateOrCreate([
            'email' => 'corebio@gmail.com'
        ],[
            'competition_type_id' => '1',
            'category_id' => '3',
            'name' => 'Corebio',
            'person_name' => 'Core bio',
            'email' => 'corebio@gmail.com',
            'mobile' => '9999999991',
            'address' => 'lorem ipsum dolor sit amet',
            'pincode' => '123456',
            'ward' => 'lorem ipsum dolor',
            'password' => Hash::make('12345678'),
            'tenant_id' => $tenant->id,
        ]);
        DB::table('model_has_roles')->updateOrInsert([
            'model_type'=> 'App\Models\User',
            'model_id'=> $user->id,
        ],[
            'role_id'=> $superAdminRole->id,
            'tenant_id'=> $tenant->id
        ]);




        // Admin Seeder ##
        $adminRole = Role::updateOrCreate(['name' => 'Admin','tenant_id' => 1]);
        $permissions = Permission::pluck('id','id')->all();
        $adminRole->syncPermissions($permissions);

        $user = User::updateOrCreate([
            'email' => 'admin@gmail.com'
        ],[
            'competition_type_id' => '1',
            'category_id' => '3',
            'name' => 'User',
            'person_name' => 'User test',
            'email' => 'admin@gmail.com',
            'mobile' => '9999999992',
            'address' => 'lorem ipsum dolor sit amet',
            'pincode' => '123456',
            'ward' => 'lorem ipsum dolor',
            'password' => Hash::make('12345678'),
            'tenant_id' => $tenant->id,
        ]);
        DB::table('model_has_roles')->updateOrInsert([
            'model_type'=> 'App\Models\User',
            'model_id'=> $user->id,
        ],[
            'role_id'=> $adminRole->id,
            'tenant_id'=> $tenant->id
        ]);



        // Majhi Vasundra HOD Seeder ##
        $committeeRole = Role::updateOrCreate(['name' => 'Committee','tenant_id' => 1]);
        $permissions = Permission::pluck('id','id')->all();
        $committeeRole->syncPermissions($permissions);

        $user = User::updateOrCreate([
            'email' => 'committee@gmail.com'
        ],[
            'competition_type_id' => '1',
            'category_id' => '3',
            'name' => 'Committee',
            'person_name' => 'committee user',
            'email' => 'committee@gmail.com',
            'mobile' => '9999999993',
            'address' => 'lorem ipsum dolor sit amet',
            'pincode' => '123456',
            'ward' => 'lorem ipsum dolor',
            'password' => Hash::make('12345678'),
            'tenant_id' => $tenant->id,
        ]);
        DB::table('model_has_roles')->updateOrInsert([
            'model_type'=> 'App\Models\User',
            'model_id'=> $user->id,
        ],[
            'role_id'=> $committeeRole->id,
            'tenant_id'=> $tenant->id
        ]);
    }
}
