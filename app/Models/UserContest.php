<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserContest extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $fillable = [ 'user_id', 'department_id', 'question_id', 'option_id', 'document_1', 'document_2', 'description', 'marks_obtained' ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function selectedQuestion()
    {
        return $this->belongsTo(Question::class);
    }

    public function selectedOption()
    {
        return $this->belongsTo(Option::class);
    }
}
