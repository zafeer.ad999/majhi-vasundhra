<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CompetitionType;
use App\Models\Department;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{

    public function showLogin()
    {
        return view('admin.login');
    }

    public function showRegister()
    {
        $categories = Category::orderBy('name')->get();
        $competitionTypes = CompetitionType::get();

        return view('admin.register')->with(['categories'=> $categories, 'competitionTypes'=> $competitionTypes]);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ],
        [
            'username.required' => 'Please enter username',
            'password.required' => 'Please enter password',
        ]);

        if ($validator->passes())
        {
            $username = $request->username;
            $password = $request->password;
            $remember_me = $request->has('remember_me') ? true : false;

            try
            {
                $user = User::where('email', $username)->orWhere('mobile', $username)->first();

                if(!$user)
                    return response()->json(['error2'=> 'No user found with this username']);

                if($user->active_status == '0' && !$user->roles)
                    return response()->json(['error2'=> 'You are not authorized to login, contact HOD']);

                if(!auth()->attempt(['email' => $username, 'password' => $password], $remember_me))
                    return response()->json(['error2'=> 'Your entered credentials are invalid']);

                $userType = '';
                if( $user->hasRole(['Maker']) )
                    $userType = 'maker';

                return response()->json(['success'=> 'login successful', 'user_type'=> $userType ]);
            }
            catch(\Exception $e)
            {
                DB::rollBack();
                Log::info("login error:". $e);
                return response()->json(['error2'=> 'Something went wrong while validating your credentials!']);
            }
        }
        else
        {
            return response()->json(['error'=>$validator->errors()]);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'competition_type_id' => 'required',
            'name' => 'required|max:255',
            'person_name' => 'required|max:255',
            'email' => 'required|email|unique:users,email',
            'mobile' => 'required|digits:10|unique:users,mobile',
            'address' => 'required',
            'pincode' => 'required|digits:6',
            'ward' => 'required',
            'password' => 'required|min:8',
            'confirm_password' => 'required|same:password',
        ]);

        if ($validator->fails())
            return response()->json(['error'=>$validator->errors()]);

        try
        {
            $input = $request->toArray();

            $input['password'] = Hash::make($input['password']);
            $input['tenant_id'] = '1';
            $user = new User();
            $user = User::create( Arr::only( $input, $user->getFillable() ) );

            if( $user )
            {
                DB::table('model_has_roles')->updateOrInsert([ 'model_type'=> 'App\Models\User', 'model_id'=> $user->id ],
                                [ 'role_id'=> '1', 'tenant_id'=> '1' ]);
                auth()->attempt(['email' => $user->email, 'password' => $request->password], false);
            }

            return response()->json(['success'=> 'Registration successful' ]);
        }
        catch(\Exception $e)
        {
            DB::rollBack();
            Log::info("login error:". $e);
            return response()->json(['error2'=> 'Something went wrong while registering your account!']);
        }

    }

    public function logout()
    {
        auth()->logout();

        return redirect()->route('login');
    }


    public function showChangePassword()
    {
        return view('admin.change-password');
    }


    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
        ]);

        if ($validator->passes())
        {
            $old_password = $request->old_password;
            $password = $request->password;

            try
            {
                $user = DB::table('app_users')->where('id', $request->user()->id)->first();

                if( Hash::check($old_password, $user->password) )
                {
                    DB::table('app_users')->where('id', $request->user()->id)->update(['password'=> Hash::make($password)]);

                    return response()->json(['success'=> 'Password changed successfully!']);
                }
                else
                {
                    return response()->json(['error2'=> 'Old password does not match']);
                }
            }
            catch(\Exception $e)
            {
                DB::rollBack();
                Log::info("password change error:". $e);
                return response()->json(['error2'=> 'Something went wrong while changing your password!']);
            }
        }
        else
        {
            return response()->json(['error'=>$validator->errors()]);
        }
    }

}
