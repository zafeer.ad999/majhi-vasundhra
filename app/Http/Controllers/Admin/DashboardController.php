<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Department;
use App\Models\User;
use Illuminate\Http\Request;


class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $authUser = auth()->user();
        $isAdmin = $authUser->hasRole([ 'Admin', 'Super Admin' ]);

        $departmentWiseData = [];
        $categoryWiseUsers = [];

        if( $isAdmin )
            $categoryWiseUsers = Category::withCount('users')->get();
        else
            $departmentWiseData = Department::withWhereHas('contests', fn($q)=> $q->where('user_id', $authUser->id) )
                                                ->get();

        // dd($departmentWiseData);
        return view('admin.dashboard.index')->with([
                        'departmentWiseData' => $departmentWiseData,
                        'categoryWiseUsers' => $categoryWiseUsers,
                        'isAdmin' => $isAdmin,
                    ]);
    }

}
